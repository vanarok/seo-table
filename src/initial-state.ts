import { FilterOptions } from './types'

export const filtersOptions: FilterOptions = {
  project: '2cf3c7c7-3a3e-4be2-80df-2e7d400ee34d',
  default_engine: 'gd',
  search_engines: {
    gd: 'Google | Desktop',
    gm: 'Google | Mobile',
    yd: 'Yandex | Desktop',
    ym: 'Yandex | Mobile',
  },
  filters: {
    top3: 'TOP3',
    top10: 'TOP10',
    top100: 'TOP100',
  },
  categories: {
    'f82bcdb6-4a94-43f7-a7ae-be2bdcc7e988': 'bonus',
    'b5139f5e-a85f-4df0-a472-f27e18f72d4f': 'apps',
    '49478b3a-72c3-44c7-8b21-7ac5d3c837cc': 'free',
  },
  groups: {
    'cf92b43b-5e8a-48ca-917f-07cad2f97cdf': 'up',
    '4cebe695-7b4b-44f3-83b6-0a190d10e625': 'down',
    '47a00e16-36d1-458c-80b0-315c1b4c595d': 'new',
  },
  start_date: '2022-11-27',
  end_date: '2022-11-27',
  show: '100&500&1000',
  count_pages: 10,
  token: '55a00e16-36d1-458c-80b0-315c1b4c5978',
}
