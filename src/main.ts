import { ViteSSG } from 'vite-ssg'
import { setupLayouts } from 'virtual:generated-layouts'
import Previewer from 'virtual:vue-component-preview'
import App from './App.vue'
import type { UserModule } from './types'
import generatedRoutes from '~pages'

import '@unocss/reset/tailwind.css'
import './styles/main.css'
import 'uno.css'
import { filtersOptions } from './initial-state'

const routes = setupLayouts(generatedRoutes)

export const createApp = ViteSSG(
  App,
  { routes, base: import.meta.env.BASE_URL },
  (ctx) => {
    const { app, initialState } = ctx

    if (import.meta.env.SSR) {
      initialState.filterOptions = filtersOptions
    } else {
      initialState.filterOptions = filtersOptions //only prod
      app.provide('initialState', initialState)
    }

    Object.values(
      import.meta.glob<{ install: UserModule }>('./modules/*.ts', {
        eager: true,
      }),
    ).forEach((i) => i.install?.(ctx))
    ctx.app.use(Previewer)
  },
)
