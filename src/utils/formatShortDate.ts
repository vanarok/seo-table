export const formatShortDate = (fullDate: string) => {
  const splitDate = String(fullDate).split('-')
  return `${splitDate[2]}.${splitDate[1]}`
}
