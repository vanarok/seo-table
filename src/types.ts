import { type ViteSSGContext } from 'vite-ssg'
import type { Ref } from 'vue'

export type UserModule = (ctx: ViteSSGContext) => void

export interface TableItem {
  avg_pos: string
  clicks: string
  ctr: string
  description: string
  impressions: string
  key: string
  position: Position
  rich: Rich
  title: string
  url: string
}
export interface Position {
  [index: string]: string
}
export interface Rich {
  answerBox: BottomAdsOrTopAdsOrAnswerBox
  bottom_ads?: BottomAdsOrTopAdsOrAnswerBox[] | null
  relatedQuestions?: RelatedQuestions[] | null
  relatedSearches?: string[] | null
  top_ads?: BottomAdsOrTopAdsOrAnswerBox[] | null
}
export interface BottomAdsOrTopAdsOrAnswerBox {
  snippet: string
  title: string
  url: string
}
export interface RelatedQuestions {
  answer: string
  question: string
}

export type TableList = TableItem[]

export type Sort = { column: string; order: 'ascending' | 'descending' | null }

interface SearchEngines {
  [key: string]: string
}

interface Filters {
  [key: string]: string
}

interface Categories {
  [key: string]: string
}

interface Groups {
  [key: string]: string
}

export interface FilterOptions {
  project: string
  default_engine: string
  search_engines: SearchEngines
  filters: Filters
  categories: Categories
  groups: Groups
  start_date: string
  end_date: string
  show: string
  count_pages: number
  token: string
}

export interface TableParams {
  project: string
  searchEngine: Ref<string>
  rangeDate: Ref<[Date, Date]>
  category: Ref<string[]>
  group: Ref<string[]>
  searchQuery: Ref<string[]>
  filter: Ref<string>
  sort: Ref<Sort>
  showLimit: Ref<number>
  page: Ref<number>
}
export interface SearchCompletionParams {
  project: string
  searchEngine: Ref<string>
  rangeDate: Ref<[Date, Date]>
  category: Ref<string[]>
  group: Ref<string[]>
  filter: Ref<string>
}

export interface initialState {
  filterOptions: FilterOptions
}
