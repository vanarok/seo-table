import { initialState } from '~/types'

export const useFilters = () => {
  const { filterOptions } = inject('initialState') as initialState
  const {
    project,
    search_engines,
    start_date,
    end_date,
    categories,
    groups,
    filters,
    show,
  } = filterOptions

  const searchEngines = search_engines
  const searchEngine = ref(Object.keys(searchEngines)[0])

  const category = ref([Object.keys(categories)[0]])

  const rangeDate = ref<[Date, Date]>([
    new Date(start_date),
    new Date(end_date),
  ])

  const group = ref([Object.keys(groups)[0]])

  const filter = ref('all')

  const sort = ref()
  const updateSort = (event: any) => {
    sort.value = { column: event.prop, order: event.order }
  }

  const showLimits = show.split('&').map((value) => Number(value))
  const showLimit = ref(showLimits[0])

  const page = ref(1)

  const searchQuery = ref([])

  return {
    project,
    searchEngine,
    searchEngines,
    rangeDate,
    category,
    categories,
    group,
    groups,
    filter,
    filters,
    sort,
    updateSort,
    showLimit,
    showLimits,
    page,
    searchQuery,
  }
}
