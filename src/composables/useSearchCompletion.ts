import type { SearchCompletionParams, TableItem } from '../types'
import axios from 'axios'

export const useSearchCompletion = (options: SearchCompletionParams) => {
  const completions = ref<TableItem[]>([])
  const error = ref<unknown>(null)
  const loading = ref<boolean>(true)
  const searchQuery = ref('')

  const updateCompletions = (query: string) => {
    searchQuery.value = query
  }

  const doFetch = async () => {
    loading.value = true
    const body = new FormData()
    const url = 'https://srvrt.ru/seo/api/v1/data'
    const headers = {
      headers: { 'Content-Type': 'multipart/form-data' },
    }

    const project = unref(options.project)
    const searchEngine = unref(options.searchEngine)
    const startDate = formatDate(unref(options.rangeDate)[0])
    const endDate = formatDate(unref(options.rangeDate)[1])
    const category = unref(options.category)
    const group = unref(options.group)
    const query = unref(searchQuery)
    const filter = unref(options.filter)

    body.append('project', String(project))
    body.append('search_engine', searchEngine)
    body.append('start_date', startDate)
    body.append('end_date', endDate)
    category.length > 0 && body.append('category_id', category.join('&'))
    group.length > 0 && body.append('group_id', group.join('&'))
    query.length && body.append('search_query', query)
    filter && filter !== 'all' && body.append('filter', String(filter))
    body.append('token', '55a00e16-36d1-458c-80b0-315c1b4c5978')

    try {
      const { data } = await axios.post(url, body, headers)

      if (searchQuery.value) {
        completions.value = data[0].filter((tableItem: TableItem) => {
          console.log(
            tableItem.key.toLowerCase(),
            searchQuery.value.toLowerCase(),
          )

          return tableItem.key
            .toLowerCase()
            .includes(searchQuery.value.toLowerCase())
        })
      } else {
        completions.value = []
      }
    } catch (err) {
      error.value = error
    }
    loading.value = false
  }

  watch(searchQuery, () => {
    doFetch()
  })

  return { data: completions, error, loading, updateCompletions }
}
