import type { TableList, TableParams } from '../types'
import axios from 'axios'

export const useTable = (options: TableParams) => {
  const table = ref<TableList>([])
  const error = ref<unknown>(null)
  const loading = ref<boolean>(true)
  const countPages = ref<number>(1)

  const doFetch = async () => {
    loading.value = true
    const body = new FormData()
    const url = 'https://srvrt.ru/seo/api/v1/data'
    const headers = {
      headers: { 'Content-Type': 'multipart/form-data' },
    }

    const project = unref(options.project)
    const searchEngine = unref(options.searchEngine)
    const startDate = formatDate(unref(options.rangeDate)[0])
    const endDate = formatDate(unref(options.rangeDate)[1])
    const category = unref(options.category)
    const group = unref(options.group)
    const searchQuery = unref(options.searchQuery)
    const filter = unref(options.filter)
    const sort = unref(options.sort)
    const typeSortes = {
      descending: 'DESC',
      ascending: 'ASC',
    }
    const show = unref(options.showLimit)
    const page = unref(options.page)

    body.append('project', String(project))
    body.append('search_engine', searchEngine)
    body.append('start_date', startDate)
    body.append('end_date', endDate)
    category.length > 0 && body.append('category_id', category.join('&'))
    group.length > 0 && body.append('group_id', group.join('&'))
    searchQuery.length && body.append('search_query', searchQuery.join('&'))
    filter && filter !== 'all' && body.append('filter', String(filter))
    sort?.order && body.append('sort', String(sort.column))
    sort?.order && body.append('type_sort', String(typeSortes[sort.order]))
    body.append('show', String(show))
    body.append('num_page', String(page))
    body.append('token', '55a00e16-36d1-458c-80b0-315c1b4c5978')

    try {
      const { data } = await axios.post(url, body, headers)
      table.value = data[0]
      countPages.value = data[1].count_page
    } catch (err) {
      error.value = error
    }
    loading.value = false
  }

  watchEffect(() => {
    const resetValues = [
      options.filter.value,
      options.showLimit.value,
      options.sort.value?.order,
    ]
    if (resetValues.every((resetValue) => !!resetValue)) {
      options.page.value = 1
    }
  })

  watchEffect(doFetch)

  return { data: table, error, loading, countPages }
}
